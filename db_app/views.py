from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from db_app import models
from . import serializers


import random, string

def Random_name(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))


# Create your views here.

class Userdata(APIView):
    def get(selfs,request):
        data = request.query_params.get('data',None)
        info,choice = data.split("|")
        if choice == "id":
            print("-----------------------")
            print(data)
            user = models.users.objects.filter(device_id=info)
        else :# choice == ip
            user = models.users.objects.filter(ip=info)
        if len(user) == 0:
            return Response({"message":"not found"},status=status.HTTP_404_NOT_FOUND)

        usr = user[0]
        return Response({"location": usr.location,"device_id": usr.device_id
                         ,"param": usr.love_param,"ip":usr.ip}, status=status.HTTP_200_OK)


    def post(self,request):

        info = request.data
        print(info)
        print("0000000000000000000000000")
        if info["command"] == "new":
            device_name = Random_name(10)
            usr = models.users(love_param=info["param"],
                               location="null",
                               device_id=device_name,
                               ip=info["ip"])
            usr.save()

            return Response({"device_id": usr.device_id})

        elif info["command"] == "update":
            user = models.users.objects.filter(device_id=info["id"])
            usr = user[0]
            usr.location = info["location"]
            usr.last_updates = info["last_updates"]
            usr.save()
            return Response({"device_id": usr.device_id, "love_param": usr.love_param})
        elif info["command"] == "delete":
            user = models.users.objects.filter(device_id=info["id"])
            user.delete()
            return Response({"message":"the user has been deleted"})


class AllUserData(APIView):
    def get(self,request):
        users = models.users.objects.all()
        ser = serializers.UserSerializer(users,many=True)
        return Response(ser.data,status=status.HTTP_200_OK)

class SelfDestruct(APIView):
    def get(self,request):
        users = models.users.objects.all()
        for user in users:
            user.delete()

        return Response({"message":"database has been cleared"})