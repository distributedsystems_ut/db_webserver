from django.db import models
import time
# Create your models here.

class users(models.Model):
    location = models.TextField(default="")
    device_id = models.TextField(default="")
    love_param = models.TextField(default="no$no$no$no")
    ip = models.TextField(default="")
    last_updates = models.FloatField(default=9999999999)
    created_time_lap = models.FloatField(default=time.time())
    # other love factors

    class Meta:
        ordering = ['-last_updates']

    def __str__(self):
        return "{}".format(self.device_id)