if you are starting up this web server for the first time you need to execute these commands first : 
```python manage.py makemigrations```
```python manage.py migrate```
```python manage.py runserver```

other wise to run the server just start the engine with the last command :
```python manage.py runserver```

PAY ATTENTION THAT IN THE CURRENT VERSION OF THIS SYSTEM DATA BASE WEBSERVER AND COMPUTATIONAL NODES ARE SUPPOSE TO BE ON THE SAME COMPUTER.